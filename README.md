# ghost2toot
Toot your Ghost's blog entries to your favorite Mastodon server

### Dependencies

-   **Python 3**
-   Postgresql server
-   Ghost's blog account

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install required Python libraries.

2. Run `python db-setup.py` to create needed database and table. It's needed to control what blog feeds are already tooted. db-setup.py
   will also ask you the Ghost's feed url.

3. Run `python setup.py` to get your bot's access token of a Mastodon existing account. It will be saved to 'secrets/secrets.txt' for further use.

4. Run `python ghost2toot.py` to start tooting your Ghost's blog feeds.

5. Use your favourite scheduling method to set ghost2toot.py to run regularly.

