import os
import sys
import feedparser
from mastodon import Mastodon
import psycopg2
from psycopg2 import errors
import datetime

###############################################################################
# INITIALISATION
###############################################################################

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

########################################################
# main

if __name__ == '__main__':

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/db_config.txt"
    feeds_db = get_parameter("feeds_db", config_filepath)
    feeds_db_user = get_parameter("feeds_db_user", config_filepath)
    feeds_url = get_parameter("feeds_url", config_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    ########################################################

    publish = 0
    prev_feed_date = None
    days = 0
    seconds = None

    blogfeeds = feedparser.parse(feeds_url)

    blogfeeds_id = []
    blogfeeds_title = []
    blogfeeds_link = []
    blogfeeds_published = []
    blogfeeds_author = []

    for entry in reversed(blogfeeds.entries):

        title = entry['title']
        id = entry['id']
        link = entry['link']
        published = entry['published']
        author = entry['author']

        try:

            conn = None
            conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            cur.execute('select id from feeds where id=(%s)', (id,))

            row = cur.fetchone()
            if row == None:
                blogfeeds_id.append(id)
                blogfeeds_title.append(title)
                blogfeeds_link.append(link)
                blogfeeds_published.append(published)
                blogfeeds_author.append(author)

            cur.close()

        except (Exception, psycopg2.errors.UndefinedColumn) as column_error:

            print(column_error)

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    ###########################################################

    i = 0
    while i < len(blogfeeds_id):

        feed_published = datetime.datetime.strptime(blogfeeds_published[i], '%a, %d %b %Y %H:%M:%S GMT')

        title = blogfeeds_title[i]
        id = blogfeeds_id[i]
        link = blogfeeds_link[i]
        author = blogfeeds_author[i]

        toot_text = str(title)+'\n'
        toot_text += str(link)+'\n'
        toot_text += '\n'
        toot_text += "Autor: " + str("@")+str(author)
        toot_text += '\n'
        toot_text += '#blog'

        print("Tooting...")
        print(toot_text)

        toot_id = mastodon.status_post(toot_text, in_reply_to_id=None,)

        #########################################################

        insert_line = 'INSERT INTO feeds(id, published, tootid) VALUES (%s,%s,%s) ON CONFLICT DO NOTHING'

        conn = None

        try:

            conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            cur.execute(insert_line, (id, feed_published, toot_id['id']))
            cur.execute('update feeds set published=(%s), tootid=(%s) where id=(%s)', (feed_published, toot_id['id'], id,))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        i += 1

        print("No new feeds")
